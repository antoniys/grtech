@extends('template')

@section('content')
@if ($message = Session::get('success'))
<div class="alert alert-success">
	<p>{{ $message }}</p>
</div>
@endif
<div class="modal fade" id="modal-default2">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Data Company</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="company_data">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<div class="form-group">
								<strong>Name:</strong>
								<input type="text" readonly name="name" class="form-control" id="company_name" placeholder="Input Company Name">
							</div>
							<div class="form-group">
								<strong>Email:</strong>
								<input type="text" readonly name="email" class="form-control" id="company_email" placeholder="Input Company Email">
							</div>
							<div class="form-group">
								<strong>Logo: <small id="logo">d</small></strong>
							</div>
							<div class="form-group">
								<strong>Website:</strong>
								<input type="text" readonly name="website" class="form-control" id="company_website" placeholder="Input Company Name">
							</div>
						</div>
					</div>
					<div class="modal-footer justify-content-between">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>

		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-default">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Employee</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="employee_data" method="POST" enctype="multipart/form-data">
					@csrf
					@method('PUT')

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<div class="form-group">
								<strong>First Name:</strong>
								<input type="text" name="firstname" class="form-control" id="firstname" placeholder="Input Employee First Name">
								<input type="hidden" name="id" id="id">
							</div>
							<div class="form-group">
								<strong>Last Name:</strong>
								<input type="text" name="lastname" class="form-control" id="lastname" placeholder="Input Employee Last Name">
							</div>
							<div class="form-group">
								<strong>Email:</strong>
								<input type="text" name="email" class="form-control" id="email" placeholder="Input Employee Email">
							</div>
							<div class="form-group">
								<strong>Company:</strong>
								<select name="company" id="company" class="form-control">
									@foreach ($companies as $company)
									<option value="{{ $company->id }}">{{ $company->name }} </option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<strong>Phone:</strong>
								<input type="text" name="phone" class="form-control" id="phone" placeholder="Input Employee Phone">
							</div>
						</div>
					</div>
					<div class="modal-footer justify-content-between">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" id="btn_update" class="btn btn-primary">Update Company</button>
					</div>
				</form>
			</div>

		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="card">
	<div class="card-header">
		<div class="float-left">
			<h2>Data Employees </h2>
		</div>
		<div class="float-right">
			<a class="btn btn-success" href="{{ route('employee.create') }}"> Add Employee</a>
		</div>
	</div>
	<!-- /.card-header -->

	<table class="table table-bordered">
		<tr>
			<th width="20px" class="text-center">No</th>
			<th>Full Name</th>
			<th>Email</th>
			<th>Company</th>
			<th>Phone</th>
			<th width="280px" class="text-center">Action</th>
		</tr>
		@foreach ($employees as $employee)
		<tr>
			<td class="text-center">{{ ++$i }}</td>
			<td>{{ $employee->firstname }} {{ $employee->lastname }}</td>
			<td>{{ $employee->email }}</td>
			<td><a class="btn btn-primary btn-sm btn_company" data-id="{{ $employee->companyid }}" data-toggle="modal" data-target="#modal-default2">{{ $employee->companyname }}</a></td>
			<td>{{ $employee->phone }}</td>
			<td class="text-center">
				<form action="{{ route('employee.destroy',$employee->id) }}" method="POST">
					<a class="btn btn-primary btn-sm btn_edit" data-id="{{ $employee->id }}" data-toggle="modal" data-target="#modal-default">Edit</a>
					@csrf
					@method('DELETE')

					<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are You Sure To Delete This Data ?')">Delete</button>
				</form>
			</td>
		</tr>
		@endforeach
	</table>

</div>
<!-- /.card-body -->
</div>

<script>
	$(document).ready(function() {
		$(".btn_edit").click(function() {
			var id = $(this).data('id');
			$.ajax({
				url: 'employee/' + id + '/edit/',
				type: 'get',
				dataType: 'json',
				success: function(data) {
					$('#company').append($('<option>', {
						value: data.data.companyid,
						text: data.data.companyname,
						selected: true
					}));

					$("#firstname").val(data.data.firstname);
					$("#lastname").val(data.data.lastname);
					$("#email").val(data.data.email);
					$("#phone").val(data.data.phone);
					$("#id").val(data.data.id);
				}
			});
		});

		$(".btn_company").click(function() {
			var id = $(this).data('id');
			$.ajax({
				url: 'company/' + id + '/edit/',
				type: 'get',
				dataType: 'json',
				success: function(data) {
					$("#company_website").val(data.data.website);
					$("#company_name").val(data.data.name);
					$("#company_email").val(data.data.email);
					$("#logo").html(data.data.logo);
				}
			});
		});

		$('#employee_data').on('submit', function(event) {
			event.preventDefault();
			var id = $("#id").val();
			var form_data = new FormData($("#employee_data")[0]);
			$.ajax({
				url: "{{ route('employee.update',1) }}",
				method: "POST",
				data: form_data,
				dataType: "json",
				processData: false,
				contentType: false,
				success: function(data) {
					if (data.success) {
						alert('Employee Successfully Updated');
						location.reload();
					} else {
						alert('Please Check Your data, First Name & Last Name is Required');
					}
				}
			})
		});
	});
</script>
@endsection