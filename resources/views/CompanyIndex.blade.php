@extends('template')

@section('content')
@if ($message = Session::get('success'))
<div class="alert alert-success">
	<p>{{ $message }}</p>
</div>
@endif
<div class="modal fade" id="modal-default">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Company</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="company_data" method="POST" enctype="multipart/form-data">
					@csrf
					@method('PUT')

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<div class="form-group">
								<strong>Name:</strong>
								<input type="text" name="name" class="form-control" id="name" placeholder="Input Company Name">
								<input type="hidden" name="id" id="id">
							</div>
							<div class="form-group">
								<strong>Email:</strong>
								<input type="text" name="email" class="form-control" id="email" placeholder="Input Company Email">
							</div>
							<div class="form-group">
								<strong>Logo: <small id="logo_view">d</small></strong>
								<input type="file" name="logo" class="form-control" id="logo" placeholder="Input Company Logo">
								<!-- <input type="hidden" name="logo_before" class="form-control" id="logo_before"> -->
							</div>
							<div class="form-group">
								<strong>Website:</strong>
								<input type="text" name="website" class="form-control" id="web_address" placeholder="Input Company Name">
							</div>
						</div>
					</div>
					<div class="modal-footer justify-content-between">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" id="btn_update" class="btn btn-primary">Update Company</button>
					</div>
				</form>
			</div>

		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="card">
	<div class="card-header">
		<div class="float-left">
			<h2>Data Companies</h2>
		</div>
		<div class="float-right">
			<a class="btn btn-success" href="{{ route('company.create') }}"> Add Company</a>

		</div>
	</div>
	<!-- /.card-header -->

	<table class="table table-bordered">
		<tr>
			<th width="20px" class="text-center">No</th>
			<th>Name</th>
			<th>Email</th>
			<th>Logo</th>
			<th>Website</th>
			<th width="280px" class="text-center">Action</th>
		</tr>
		@foreach ($companies as $company)
		<tr>
			<td class="text-center">{{ ++$i }}</td>
			<td>{{ $company->name }}</td>
			<td>{{ $company->email }}</td>
			<td><a target="_blank" href="{{Storage::url($company->logo)}}">See Logo</a></td>
			<td><a target="_blank" href='//{{ $company->website }}'> {{ $company->website }} </a></td>
			<td class="text-center">
				<form action="{{ route('company.destroy',$company->id) }}" method="POST">
					<a class="btn btn-primary btn-sm btn_edit" data-id="{{ $company->id }}" data-toggle="modal" data-target="#modal-default">Edit</a>
					@csrf
					@method('DELETE')

					<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are You Sure To Delete This Data ?')">Delete</button>
				</form>
			</td>
		</tr>
		@endforeach
	</table>

	{!! $companies->links() !!}
</div>
<!-- /.card-body -->
</div>
<script>
	$(document).ready(function() {
		$(".btn_edit").click(function() {
			var id = $(this).data('id');
			$.ajax({
				url: 'company/' + id + '/edit/',
				type: 'get',
				dataType: 'json',
				success: function(data) {
					$("#web_address").val(data.data.website);
					$("#name").val(data.data.name);
					$("#email").val(data.data.email);
					$("#logo_before").val(data.data.logo);
					$("#logo_view").html(data.data.logo);
					$("#id").val(data.data.id);
					$("#logo").val(data.data.logo);
				}
			});
		});

		$('#company_data').on('submit', function(event) {
			event.preventDefault();
			var id = $("#id").val();
			var form_data = new FormData($("#company_data")[0]);
			$.ajax({
				url: "{{ route('company.update',1) }}",
				method: "POST",
				data: form_data,
				dataType: "json",
				processData: false,
				contentType: false,
				success: function(data) {
					if(data.success)
					{
						alert('Company Successfully Updated');
						location.reload();
					}else{
						alert('Please Check Your data, Name is required & logo must image file');
					}
				}
			})
		});
	});
</script>

@endsection