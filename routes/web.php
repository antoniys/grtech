<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['auth']], function() {
    Route::resource('company', CompanyController::class)->middleware('checkUser');;
    Route::resource('employee', EmployeeController::class)->middleware('checkUser');
    Route::get('/user', function () {
        return view('usernotadmin');
    });
});

require __DIR__.'/auth.php';
